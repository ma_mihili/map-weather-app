package com.example.weatherapp;

public class WeatherModel {
    private String name;
    private Main main;
    private Weather[] weather;

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public Weather[] getWeather() {
        return weather;
    }

    public void setWeather(Weather[] weather) {
        this.weather = weather;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public class Main {
        private double temp;
        private int humidity;
        private double tempMin;
        private double tempMax;

        public double getTemp() {
            return temp;
        }

        public void setTemp(double temp) {
            this.temp = temp;
        }

        public int getHumidity() {
            return humidity;
        }

        public void setHumidity(int humidity) {
            this.humidity = humidity;
        }

        public double getTempMin() {
            return tempMin;
        }

        public void setTempMin(double tempMin) {
            this.tempMin = tempMin;
        }

        public double getTempMax() {
            return tempMax;
        }

        public void setTempMax(double tempMax) {
            this.tempMax = tempMax;
        }
    }

    public class Weather {
        private int id;
        private String main;
        private String description;
        private String icon;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getMain() {
            return main;
        }

        public void setMain(String main) {
            this.main = main;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getIcon() {
            switch(icon) {
                case "02d":
                    return R.drawable.ic_02d;
                case "03d":
                case "04":
                    return R.drawable.ic_03d;
                case "09":
                    return R.drawable.ic_09d;
                case "10d":
                    return R.drawable.ic_10d;
                case "11d":
                    return R.drawable.ic_11n;
                case "13d":
                    return R.drawable.ic_13d;
                case "50d":
                    return R.drawable.ic_50d;
                case "01d":
                default:
                    return R.drawable.ic_01d;
            }
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }
    }
}
