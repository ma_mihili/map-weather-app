package com.example.weatherapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class CurrentFragment extends Fragment {
    private OkHttpClient client = new OkHttpClient();
    private Request request;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View w = inflater.inflate(R.layout.fragment_current, container, false);
        MainActivity ma = (MainActivity)getActivity();

        request = new Request.Builder()
                .url("http://api.openweathermap.org/data/2.5/weather?q=" + ma.city + "&appid=45241fc43340385f1741c0f12a8e2d82&units=metric")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                try {
                    String responseString = response.body().string();
                    JSONObject jsonObject = new JSONObject(responseString);
                    System.out.print(jsonObject);
                    Gson gson = new Gson();
                    final WeatherModel weatherData = gson.fromJson(jsonObject.toString(), WeatherModel.class);

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateUi(w, weatherData);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        return w;
    }

    public void updateUi(View w, WeatherModel weather) {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        TextView nameView = w.findViewById(R.id.current_name);
        TextView updateView = w.findViewById(R.id.current_update);
        ImageView iconView = w.findViewById(R.id.current_icon);
        TextView descriptionView = w.findViewById(R.id.current_description);
        TextView humidityView = w.findViewById(R.id.current_humidity);
        TextView tempView = w.findViewById(R.id.current_temp);

        nameView.setText(weather.getName());
        updateView.setText("Last updated at: " + formatter.format(date));
        iconView.setBackgroundResource(weather.getWeather()[0].getIcon());
        descriptionView.setText(weather.getWeather()[0].getDescription());
        humidityView.setText("Humidity: " + weather.getMain().getHumidity());
        tempView.setText(weather.getMain().getTemp() + "C");
    }
}
