package com.example.weatherapp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ForecastFragment extends Fragment {
    private OkHttpClient client = new OkHttpClient();
    private Request request;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View w = inflater.inflate(R.layout.fragment_forecast, container, false);
        MainActivity ma = (MainActivity)getActivity();
        final ListView lw = w.findViewById(R.id.forecast_list);

        request = new Request.Builder()
                .url("http://api.openweathermap.org/data/2.5/forecast/daily?q=" + ma.city + "&appid=45241fc43340385f1741c0f12a8e2d82&units=metric")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                try {
                    String responseString = response.body().string();
                    JSONObject jsonObject = new JSONObject(responseString);
                    System.out.print(jsonObject);
                    Gson gson = new Gson();
                    final ForecastModel weatherData = gson.fromJson(jsonObject.toString(), ForecastModel.class);

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateUi(w, lw, weatherData);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        return w;
    }

    public void updateUi(View v, ListView lw, ForecastModel fm) {
        MyAdapter adapter = new MyAdapter(getContext(), fm.list);
        lw.setAdapter(adapter);
    }
}

class MyAdapter extends ArrayAdapter<List> {
    Context context;
    List rList[];

    MyAdapter(Context c, List list[]) {
        super(c, R.layout.forecast_listitem, R.id.forecast_name, list);
        this.context = c;
        this.rList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater ly = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = ly.inflate(R.layout.forecast_listitem, parent, false);
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd");
        Long dv = Long.valueOf(rList[position].dt) * 1000;
        Date dt = new Date(dv);

        TextView name = row.findViewById(R.id.forecast_name);
        TextView min = row.findViewById(R.id.forecast_min);
        TextView max = row.findViewById(R.id.forecast_max);
        ImageView image = row.findViewById(R.id.forecast_icon);

        min.setText(rList[position].temp.min + "C");
        max.setText(rList[position].temp.max + "C");
        name.setText(formatter.format(dt));

        image.setBackgroundResource(rList[position].weather[0].getIcon());

        return row;
    }
}
