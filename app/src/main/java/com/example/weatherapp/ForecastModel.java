package com.example.weatherapp;

public class ForecastModel {
    public City city;
    public List[] list;

    class City {
        public int id;
        public String name;
    }

}

class List {
    public String dt;
    public Temp temp;
    public Weather[] weather;
}

class Temp {
    public String min;
    public String max;
}

class Weather {
    public String main;
    public String description;
    public String icon;

    public int getIcon() {
        switch(icon) {
            case "02d":
                return R.drawable.ic_02d;
            case "03d":
            case "04":
                return R.drawable.ic_03d;
            case "09":
                return R.drawable.ic_09d;
            case "10d":
                return R.drawable.ic_10d;
            case "11d":
                return R.drawable.ic_11n;
            case "13d":
                return R.drawable.ic_13d;
            case "50d":
                return R.drawable.ic_50d;
            case "01d":
            default:
                return R.drawable.ic_01d;
        }
    }
}
