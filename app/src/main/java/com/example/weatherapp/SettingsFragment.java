package com.example.weatherapp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SettingsFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View w = inflater.inflate(R.layout.fragment_settings, container, false);

        final MainActivity ma = (MainActivity) getActivity();

        EditText input = w.findViewById(R.id.settings_input);
        input.setText(ma.city);

        Button saveButton = w.findViewById(R.id.settings_save);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "City changed", Toast.LENGTH_SHORT);

                EditText input = getActivity().findViewById(R.id.settings_input);
                ma.city = input.getText().toString();
            }
        });

        return w;
    }
}
