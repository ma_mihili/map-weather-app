# Android Weather App

Simple weather application for school project. Weather data is fetched from [Open Weather Map](https://openweathermap.org) api and displayed in the app. 

### Functionalities
1. display current weather for selected city
2. display week weather for selected city
3. change selected city
Note: default city is `London`


### Third party libs used
* [okhttp](https://square.github.io/okhttp/)
* [gson](https://github.com/google/gson)
* [picasso](https://square.github.io/picasso/)
